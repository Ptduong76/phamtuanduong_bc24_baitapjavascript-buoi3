/** Bài tập về nhà số 6: Xác định sinh viên ở xa trường đại học nhất
 *
 * input: Tên và toạ dộ nhà của 3 sinh viên, toạ độ trường học
 *
 * process:
 *    1. Tạo các biến input: name, location
 *    2. Tính khoảng cách từ nhà của từng sinh viên tới trường
 *     2.1. Coi nhà của các sinh viên và trường học là các điểm trên trục toạ độ Oxy
 *     2.2. Đặt vị trí trường học tại gốc toạ độ [0,0]
 *     2.3. Toạ độ nhà của các sinh viên được xác định bằng giá trị x và y trên trục toạ độ Oxy
 *     2.4. Khoảng cách từ nhà sinh viên tới trường là khoảng cách từ các điểm biểu diễn vị trí nhà sinh viên tới gốc toạ độ
 *    3. So sánh các khoảng cách vừa tính được
 *    4. Chọn khoảng cách xa nhất và tìm tên sinh viên tương ứng
 *
 * output: Xuất ra tên sinh viên ở xa trường đại học nhất
 */

function sortLongestDistance() {
  var student1 = "cyberSoft1";
  var student2 = "cyberSoft2";
  var student3 = "cyberSoft3";

  var locationStudent1 = [7, 23];
  var locationStudent2 = [-5, 12];
  var locationStudent3 = [10, -7];

  var distanceStudent1 = Math.sqrt(
    locationStudent1[0] * locationStudent1[0] +
      locationStudent1[1] * locationStudent1[1]
  );
  var distanceStudent2 = Math.sqrt(
    locationStudent2[0] * locationStudent2[0] +
      locationStudent2[1] * locationStudent2[1]
  );
  var distanceStudent3 = Math.sqrt(
    locationStudent3[0] * locationStudent3[0] +
      locationStudent3[1] * locationStudent3[1]
  );

  if (distanceStudent1 <= distanceStudent2) {
    if (distanceStudent3 <= distanceStudent1) {
      console.log("Sinh viên ở xa trường nhất:", student2);
    } else {
      if (distanceStudent3 <= distanceStudent2) {
        console.log("Sinh viên ở xa trường nhất:", student2);
      } else {
        console.log("Sinh viên ở xa trường nhất:", student3);
      }
    }
  } else {
    if (distanceStudent3 <= distanceStudent2) {
      console.log("Sinh viên ở xa trường nhất:", student1);
    } else {
      if (distanceStudent3 <= distanceStudent1) {
        console.log("Sinh viên ở xa trường nhất:",student1);
      } else {
        console.log("Sinh viên ở xa trường nhất:",student3);
      }
    }
  }
}

sortLongestDistance();
