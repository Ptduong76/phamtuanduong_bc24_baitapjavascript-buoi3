/** Bài tập về nhà số 3:
 *
 * input: Nhập vào câu trả lời là một trong 4 thành viên của gia đình
 *
 * process:
 *    1. Tạo biến câu trả lời nhận được từ input: answer
 *       Tạo các biến câu chào dành cho Bố (helloDad), Mẹ (helloMom), Anh trai (helloBigBro), Em gái (helloLitSis)
 *    2. Căn cứ vào câu trả lời nhận được từ input có chứa chuỗi: Bố, Mẹ, Anh trai, Em gái rồi đưa ra câu chào hợp lý
 *
 * output: Xuất ra câu trả lời phù hợp
 */

function greetFamillyMembers() {
  var answer = "bố đây";

  var helloDad = "Chào Bố";
  var helloMom = "Chào Mẹ";
  var helloBigBro = "Chào Anh trai";
  var helloLitSis = "Chào Em gái";

  if (answer.indexOf("Bố") > -1 || answer.indexOf("bố") > -1) {
    console.log(helloDad);
  } else if (answer.indexOf("Mẹ") > -1 || answer.indexOf("mẹ") > -1) {
    console.log(helloMom);
  } else if (answer.indexOf("Anh trai") > -1 || answer.indexOf("anh trai") > -1) {
    console.log(helloBigBro);
  } else if (answer.indexOf("Em gái") > -1 || answer.indexOf("em gái") > -1) {
    console.log(helloLitSis);
  } else {
    console.log("Không quen nhá");
  }
}

greetFamillyMembers();
