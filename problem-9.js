/** Bài tập về nhà số 9: Xác định loại tam giác
 *
 * input: Nhập vào giá trị 3 cạnh của tam giác
 *
 * process:
 *    1. Tạo các biến input: các cạnh của tam giác edge0, edge1, edge2
 *    2. Đảm bảo độ dài các cạnh hợp lệ: tổng 2 cạnh bất kì lớn hơn cạnh còn lại, nếu không hợp lệ => trả lỗi
 *    3. Xét trường hợp các loại tam giác đặc biệt => đưa ra điều kiện giá trị phù hợp, còn lại là tam giác thường
 *
 * output: Xuất ra loại tam giác ứng với 3 giá trị
 */

function checkValidTriangle(edge0, edge1, edge2) {
  var edge0;
  var edge1;
  var edge2;

  if (edge0 + edge1 > edge2 && edge0 + edge2 > edge1 && edge1 + edge2 > edge0) {
    return true;
  } else {
    return false;
  }
}

function checkIsoscelesTriangle(edge0, edge1, edge2) {
  var edge0;
  var edge1;
  var edge2;

  if (edge0 === edge1 || edge0 === edge2 || edge1 === edge2) {
    return true;
  } else {
    return false;
  }
}

function checkRightTriangle(edge0, edge1, edge2) {
  var edge0;
  var edge1;
  var edge2;

  if (
    edge0 * edge0 + edge1 * edge1 === edge2 * edge2 ||
    edge0 * edge0 + edge2 * edge2 === edge1 * edge1 ||
    edge2 * edge2 + edge1 * edge1 === edge0 * edge0
  ) {
    return true;
  } else {
    return false;
  }
}

function checkObtuseTriangle(edge0, edge1, edge2) {
  var edge0;
  var edge1;
  var edge2;

  if (
    edge0 * edge0 + edge1 * edge1 < edge2 * edge2 ||
    edge0 * edge0 + edge2 * edge2 < edge1 * edge1 ||
    edge2 * edge2 + edge1 * edge1 < edge0 * edge0
  ) {
    return true;
  } else {
    return false;
  }
}

function checkTriangleType() {
  var edge0 = 6;
  var edge1 = 4;
  var edge2 = 5;

  if (checkValidTriangle(edge0, edge1, edge2) !== true) {
    console.log("Các giá trị nhập vào không phải 3 cạnh của một tam giác");
    return;
  }

  if (checkIsoscelesTriangle(edge0, edge1, edge2) === true) {
    if (checkObtuseTriangle(edge0, edge1, edge2) === true) {
      console.log("Tam giác tù cân");
    } else if (checkRightTriangle(edge0, edge1, edge2 === true)) {
      console.log("Tam giác vuông cân");
    } else if (edge0 === edge1 && edge0 === edge2) {
      console.log("Tam giác đều");
    } else {
      console.log("Tam giác cân");
    }
  } else if (checkRightTriangle(edge0, edge1, edge2) === true) {
    console.log("Tam giác vuông");
  } else if (checkObtuseTriangle(edge0, edge1, edge2) === true) {
    console.log("Tam giác tù");
  } else {
    console.log("Tam giác nhọn");
  }
}

checkTriangleType();
