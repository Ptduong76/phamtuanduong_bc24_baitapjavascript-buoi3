// Bài tập trên lớp 1: Quản lý điểm sinh viên

/**
 * input:
 *      điểm chuẩn của hội đồng:
 *      điểm 3 môn thi: toán, lý, hoá
 *      khu vực:
 *      đối tượng dự thi:
 *
 *
 * process:
 *      1. tạo biến input: standardGrade, mathGrade, physicsGrade, chemistryGrade, area, type.
 *      2. kiểm tra 1 trong 3 môn 0 điểm => rớt
 *      3. gán điểm ưu tiên khu vực. area => areaGrade
 *      4. gán điểm ưu tiên đối tượng. type => typeGrade
 *      5. tính tổng điểm
 *      6. kiểm tra kết quả: tổng điểm >= điểm chuẩn => đậu, ngược lại rớt
 *      7. in kết quả ra màn hình
 *
 * output:
 *      xuất ra tổng điểm đạt được
 *      kiểm tra thí sinh đậu hay rớt
 */

// function: nhận vào khu vực => điểm khu vực
function calcAreaGrade(area) {
  switch (area) {
    case "A":
      return 2;
    case "B":
      return 1;
    case "C":
      return 0.5;
    default:
      return 0;
  }
}

// function: nhận vào loại đối tượng => điểm loại đối tượng
function calcTypeGrade(type) {
  switch (type) {
    case "1":
      return 2.5;
    case "2":
      return 1.5;
    case "3":
      return 1;
    default:
      return 0;
  }
}

function checkExamResult() {
  var standardGrade = 22;

  var mathGrade = 2;
  var physicsGrade = 1;
  var chemistryGrade = 5;

  var area = "A";
  var type = "2";

  var areaGrade;
  var typeGrade;

  var totalGrade;

  if (mathGrade === 0 || physicsGrade === 0 || chemistryGrade === 0) {
    console.log("Trượt cmnr");
    return;
  }

  //tính điểm khu vực
  areaGrade = calcAreaGrade(area);

  //tính điểm loại đối tượng
  typeGrade = calcTypeGrade(type);

  totalGrade =
    mathGrade + physicsGrade + chemistryGrade + areaGrade + typeGrade;

  if (totalGrade >= standardGrade) {
    console.log("Tổng điểm:", totalGrade, "Đậu rồi hên vl");
  } else {
    console.log("Tổng điểm:", totalGrade, "Trượt cmnr");
  }
}

checkExamResult();

function classifyStudent() {
  var standardGrade = 22;

  var mathGrade = 2;
  var physicsGrade = 1;
  var chemistryGrade = 5;

  var area = "A";
  var type = "2";

  var areaGrade;
  var typeGrade;

  var totalGrade;

  totalGrade =
    mathGrade + physicsGrade + chemistryGrade + areaGrade + typeGrade;

  if (totalGrade >= 25) {
    console.log("Lớp chất lượng cao");
  } else if (totalGrade >= standardGrade && totalGrade <= 25) {
    console.log("Lớp thường");
  } else {
    console.log("Trượt cmnr");
  }
}

classifyStudent();

/** Bài tập trên lớp 2: Tính tiền điện
 *
 * input: Thông tin tiêu thụ điện: Tên, Số Kw
 *
 * process:
 *      1. Tạo biến input: name, electricConsumed
 *      2. So sánh mức tiêu thụ với các mốc 50kw, 100kw, 200kw
 *      3. Sử dụng câu điều kiện để tính tổng số tiền cần trả
 *      4. In kết quả ra màn hình
 *
 * output: Xuất ra số tiền cần trả
 */

function calcElectricConsumed() {
  var name = "Cybersoft";

  var firstStepAmount = 50;
  var firstStepPrice = 500;

  var secondStepAmount = firstStepAmount + 50;
  var secondStepPrice = 650;

  var thirdStepAmount = secondStepAmount + 100;
  var thirdStepPrice = 850;

  var fourthStepAmount = thirdStepAmount + 150;
  var fourthStepPrice = 1100;

  var lastStepAmount;
  var lastStepPrice = 1300;

  var electricConsumed = 474;

  var totalElectricPrice;

  if (electricConsumed <= firstStepAmount) {
    totalElectricPrice = electricConsumed * firstStepPrice;
  } else if (
    electricConsumed > firstStepAmount &&
    electricConsumed <= secondStepAmount
  ) {
    totalElectricPrice =
      firstStepAmount * firstStepPrice +
      (electricConsumed - firstStepAmount) * secondStepPrice;
  } else if (
    electricConsumed > secondStepAmount &&
    electricConsumed <= thirdStepAmount
  ) {
    totalElectricPrice =
      firstStepAmount * firstStepPrice +
      (secondStepAmount - firstStepAmount) * secondStepPrice +
      (electricConsumed - secondStepAmount) * thirdStepPrice;
  } else if (
    electricConsumed > thirdStepAmount &&
    electricConsumed <= fourthStepAmount
  ) {
    totalElectricPrice =
      firstStepAmount * firstStepPrice +
      (secondStepAmount - firstStepAmount) * secondStepPrice +
      (thirdStepAmount - secondStepAmount) * thirdStepPrice +
      (electricConsumed - thirdStepAmount) * fourthStepPrice;
  } else {
    totalElectricPrice =
      firstStepAmount * firstStepPrice +
      (secondStepAmount - firstStepAmount) * secondStepPrice +
      (thirdStepAmount - secondStepAmount) * thirdStepPrice +
      (fourthStepAmount - thirdStepAmount) * fourthStepPrice +
      (electricConsumed - fourthStepAmount) * lastStepPrice;
  }

  console.log(totalElectricPrice);
}

calcElectricConsumed();

/** Bài tập trên lớp 3: Tính thuế thu nhập cá nhân
 *
 * input: Thông tin của một cá nhân
 *    Họ tên: name
 *    Tổng thu nhập năm: totalIncome
 *    Số người phụ thuộc: totalDependant
 *
 * process:
 *    1. Tạo các biến input: name, totalIncome, totalDependant, taxableIncome, taxRate, totalIncomeTax
 *    2. Tính thu nhập chịu thuế
 *    3. Xác định mức thuế suất dựa trên thu nhập chịu thuế
 *    4. Tính thuế thu nhập cá nhân
 *
 * output: Tính và xuất ra thuế thu nhập cá nhân
 *
 */

function calcIncomeTax() {
  var name = "Cybersoft";
  var totalIncome = 260000000;
  var totalDependant = 2;
  var taxableIncome;
  var taxRate;
  var totalIncomeTax;

  taxableIncome = totalIncome - 4000000 - totalDependant * 1600000;

  if (totalIncome <= 60000000) {
    taxRate = 0.05;
  } else if (totalIncome > 60000000 && totalIncome <= 120000000) {
    taxRate = 0.1;
  } else if (totalIncome > 120000000 && totalIncome <= 210000000) {
    taxRate = 0.15;
  } else if (totalIncome > 210000000 && totalIncome <= 384000000) {
    taxRate = 0.2;
  } else if (totalIncome > 384000000 && totalIncome <= 624000000) {
    taxRate = 0.25;
  } else if (totalIncome > 634000000 && totalIncome <= 960000000) {
    taxRate = 0.3;
  } else {
    taxRate = 0.35;
  }

  totalIncomeTax = taxableIncome * taxRate;

  console.log(totalIncomeTax);
}

calcIncomeTax();

/** Bài tập trên lớp 4: Tính tiền cáp
 *
 * input:
 *    Mã khách hàng:  customerNumber
 *    Loại khách hàng:  customerType
 *    Số kết nối: connections
 *    Số kênh cao cấp: exclusiveChannels
 *
 * process:
 *    1. Tạo các biến input: customerNumber, type, connections, exclusiveChannels, basicServicesFee, homeExclusiveFee, businessExclusiveFee,
 *    2. Kiểm tra loại khách hàng là nhà dân hay doanh nghiệp => nhà dân: không tính số kết nối / doanh nghiệp: tính tiền dựa trên số kết nối
 *    3. Tính tổng số tiền cáp = phí xử lý hoá đơn + phí dịch vụ cơ bản + số kết nối (nếu có) + kênh cao cấp
 *
 * output: Tính và xuất ra tiền cáp
 *
 */

function calcCableFee() {
  var customerNumber = "Cybersoft123";

  var customeType = "Doanh nghiệp";
  var connections;

  var billProcessFee;
  var basicServicesFee;

  var exclusiveChannels = 10;
  var exclusiveChannelsFee;

  var totalCableFee;

  switch (customeType) {
    case "Nhà dân":
      billProcessFee = 4.5;
      basicServicesFee = 20.5;
      exclusiveChannelsFee = exclusiveChannels * 7.5;
      totalCableFee = billProcessFee + basicServicesFee + exclusiveChannelsFee;
      break;

    case "Doanh nghiệp":
      billProcessFee = 15;
      connections = 13;
      if (connections <= 10) {
        basicServicesFee = 75;
      } else {
        basicServicesFee = 75 + (connections - 10) * 5;
      }
      exclusiveChannelsFee = exclusiveChannels * 50;
      totalCableFee = billProcessFee + basicServicesFee + exclusiveChannelsFee;
      break;

    default:
      console.log("Loại khách hàng không phù hợp");
  }

  console.log(totalCableFee);
}

calcCableFee();

/** Bài tập về nhà số 1: Sắp xếp theo thứ tự tăng dần
 *
 * input: nhập 3 số nguyên: firstNum, secondNum, thirdNum
 *
 * process:
 *    1. Tạo các biến input:
 *        số thứ nhất: firstNum
 *        số thứ hai: secondNum
 *        số thứ ba: thirdNum
 *    2. So sánh số thứ nhất với số thứ 2 => chia thành 2 trường hợp
 *    3. So sánh số cond lại với 2 đầu tiên => xét các trường hợp còn lại
 *    4. In ra kết quả sắp xếp 3 số theo thứ tự ứng với từng trường hợp
 *
 * output: xuất 3 số theo thứ tự tăng dần
 */

function sortAscending() {
  var firstNum = 19762;
  var secondNum = 4677;
  var thirdNum = 49000;

  if (firstNum <= secondNum) {
    if (thirdNum <= firstNum) {
      console.log("Thứ tự tăng dần:", thirdNum, firstNum, secondNum);
    } else {
      if (thirdNum <= secondNum) {
        console.log("Thứ tự tăng dần:", firstNum, thirdNum, secondNum);
      } else {
        console.log("Thứ tự tăng dần:", firstNum, secondNum, thirdNum);
      }
    }
  } else {
    if (thirdNum <= secondNum) {
      console.log("Thứ tự tăng dần:", thirdNum, secondNum, firstNum);
    } else {
      if (thirdNum <= firstNum) {
        console.log("Thứ tự tăng dần:", secondNum, thirdNum, firstNum);
      } else {
        console.log("Thứ tự tăng dần:", secondNum, firstNum, thirdNum);
      }
    }
  }
}

sortAscending();

/** Bài tập về nhà số 2: Tìm ngày, tháng, năm kế tiếp và liền trước
 *
 * input: Nhập ngày (day), tháng (month), năm (year)
 *
 * process:
 *    1. Tạo các biến input: curentDay, curentMonth, curentYear, nextDay, nextMonth, nextYear, previousDay, previousMonth, previousYear
 *    2. Chia các trường hợp:
 *      TH1: Tháng 5, 7, 8, 10
 *      TH2: Tháng 4,6,9,11
 *      TH3: Tháng 2
 *      TH3: Tháng 3
 *    3. Với mỗi trường hợp, xét các khả năng nếu ngày được nập là ngày 1, ngày 30/31 hay các ngày khác => Suy ra ngày kế và ngày trước đó
 *    4. Với tháng 2 và tháng 3, cần xét năm nhuận, sau đó lặp lại như trên
 *
 *
 * output: Xuất ra ngày, tháng, năm tiếp theo và trước đó
 *
 */

function leapYear(year) {
  var year;
  var leapYear;

  if (
    (year % 4 === 0 && year % 100 !== 0) ||
    (year % 100 === 0 && year % 400 === 0)
  ) {
    return (leapYear = true);
  } else {
    return (leapYear = false);
  }
}

function checkNextAndPreviousDates() {
  var currentDay = 1;
  var currentMonth = 3;
  var currentYear = 2400;

  var nextDay;
  var nextMonth;
  var nextYear;

  var previousDay;
  var previousMonth;
  var previousYear;

  if (
    currentMonth === 5 ||
    currentMonth === 7 ||
    currentMonth === 8 ||
    currentMonth === 10
  ) {
    if (currentDay === 1) {
      nextDay = 2;
      nextMonth = currentMonth;
      nextYear = currentYear;

      previousDay = 30;
      previousMonth = currentMonth - 1;
      previousYear = currentYear;
    } else if (currentDay === 31) {
      nextDay = 1;
      nextMonth = currentMonth + 1;
      nextYear = currentYear;

      previousDay = 30;
      previousMonth = currentMonth;
      previousYear = currentYear;
    } else {
      nextDay = currentDay + 1;
      nextMonth = currentMonth;
      nextYear = currentYear;

      previousDay = currentDay - 1;
      previousMonth = currentMonth;
      previousYear = currentYear;
    }
  } else if (
    currentMonth === 4 ||
    currentMonth === 6 ||
    currentMonth === 9 ||
    currentMonth === 11
  ) {
    if (currentDay === 1) {
      nextDay = 2;
      nextMonth = currentMonth;
      nextYear = currentYear;

      previousDay = 31;
      previousMonth = currentMonth - 1;
      previousYear = currentYear;
    } else if (currentDay === 30) {
      nextDay = 1;
      nextMonth = currentMonth + 1;
      nextYear = currentYear;

      previousDay = 31;
      previousMonth = currentMonth;
      previousYear = currentYear;
    } else {
      nextDay = currentDay + 1;
      nextMonth = currentMonth;
      nextYear = currentYear;

      previousDay = currentDay - 1;
      previousMonth = currentMonth;
      previousYear = currentYear;
    }
  } else if (currentMonth === 1) {
    if (currentDay === 1) {
      nextDay = 2;
      nextMonth = currentMonth;
      nextYear = currentYear;

      previousDay = 31;
      previousMonth = 12;
      previousYear = currentYear - 1;
    } else if (currentDay === 31) {
      nextDay = 1;
      nextMonth = currentMonth + 1;
      nextYear = currentYear;

      previousDay = 30;
      previousMonth = currentMonth;
      previousYear = currentYear;
    } else {
      nextDay = currentDay + 1;
      nextMonth = currentMonth;
      nextYear = currentYear;

      previousDay = currentDay - 1;
      previousMonth = currentMonth;
      previousYear = currentYear;
    }
  } else if (currentMonth === 12) {
    if (currentDay === 1) {
      nextDay = 2;
      nextMonth = currentMonth;
      nextYear = currentYear;

      previousDay = 30;
      previousMonth = currentMonth - 1;
      previousYear = currentYear;
    } else if (currentDay === 31) {
      nextDay = 1;
      nextMonth = currentMonth + 1;
      nextYear = currentYear + 1;

      previousDay = 30;
      previousMonth = currentMonth;
      previousYear = currentYear;
    } else {
      nextDay = currentDay + 1;
      nextMonth = currentMonth;
      nextYear = currentYear;

      previousDay = currentDay - 1;
      previousMonth = currentMonth;
      previousYear = currentYear;
    }
  } else if (currentMonth === 2) {
    if (leapYear(currentYear) === true) {
      if (currentDay === 1) {
        nextDay = 2;
        nextMonth = currentMonth;
        nextYear = currentYear;

        previousDay = 31;
        previousMonth = currentMonth - 1;
        previousYear = currentYear;
      } else if (currentDay === 29) {
        nextDay = 1;
        nextMonth = currentMonth + 1;
        nextYear = currentYear;

        previousDay = 28;
        previousMonth = currentMonth;
        previousYear = currentYear;
      } else {
        nextDay = currentDay + 1;
        nextMonth = currentMonth;
        nextYear = currentYear;

        previousDay = currentDay - 1;
        previousMonth = currentMonth;
        previousYear = currentYear;
      }
    } else {
      if (currentDay === 1) {
        nextDay = 2;
        nextMonth = currentMonth;
        nextYear = currentYear;

        previousDay = 31;
        previousMonth = currentMonth - 1;
        previousYear = currentYear;
      } else if (currentDay === 28) {
        nextDay = 1;
        nextMonth = currentMonth + 1;
        nextYear = currentYear;

        previousDay = 27;
        previousMonth = currentMonth;
        previousYear = currentYear;
      } else {
        nextDay = currentDay + 1;
        nextMonth = currentMonth;
        nextYear = currentYear;

        previousDay = currentDay - 1;
        previousMonth = currentMonth;
        previousYear = currentYear;
      }
    }
  } else {
    if (leapYear(currentYear) === true) {
      if (currentDay === 1) {
        nextDay = 2;
        nextMonth = currentMonth;
        nextYear = currentYear;

        previousDay = 29;
        previousMonth = currentMonth - 1;
        previousYear = currentYear;
      } else if (currentDay === 31) {
        nextDay = 1;
        nextMonth = currentMonth + 1;
        nextYear = currentYear;

        previousDay = 30;
        previousMonth = currentMonth;
        previousYear = currentYear;
      } else {
        nextDay = currentDay + 1;
        nextMonth = currentMonth;
        nextYear = currentYear;

        previousDay = currentDay - 1;
        previousMonth = currentMonth;
        previousYear = currentYear;
      }
    } else {
      if (currentDay === 1) {
        nextDay = 2;
        nextMonth = currentMonth;
        nextYear = currentYear;

        previousDay = 28;
        previousMonth = currentMonth - 1;
        previousYear = currentYear;
      } else if (currentDay === 31) {
        nextDay = 1;
        nextMonth = currentMonth + 1;
        nextYear = currentYear;

        previousDay = 30;
        previousMonth = currentMonth;
        previousYear = currentYear;
      } else {
        nextDay = currentDay + 1;
        nextMonth = currentMonth;
        nextYear = currentYear;

        previousDay = currentDay - 1;
        previousMonth = currentMonth;
        previousYear = currentYear;
      }
    }
  }

  console.log(
    "Ngày, tháng, năm tiếp theo là:",
    "Ngày",
    nextDay,
    "Tháng",
    nextMonth,
    "Năm",
    nextYear
  );
  console.log(
    "Ngày, tháng, năm trước đó là:",
    "Ngày",
    previousDay,
    "Tháng",
    previousMonth,
    "Năm",
    previousYear
  );
}

checkNextAndPreviousDates();

/** Bài tập về nhà số 3:
 *
 * input: Nhập vào câu trả lời là một trong 4 thành viên của gia đình
 *
 * process:
 *    1. Tạo biến câu trả lời nhận được từ input: answer
 *       Tạo các biến câu chào dành cho Bố (helloDad), Mẹ (helloMom), Anh trai (helloBigBro), Em gái (helloLitSis)
 *    2. Căn cứ vào câu trả lời nhận được từ input có chứa chuỗi: Bố, Mẹ, Anh trai, Em gái rồi đưa ra câu chào hợp lý
 *
 * output: Xuất ra câu trả lời phù hợp
 */

function greetFamillyMembers() {
  var answer = "bố đây";

  var helloDad = "Chào Bố";
  var helloMom = "Chào Mẹ";
  var helloBigBro = "Chào Anh trai";
  var helloLitSis = "Chào Em gái";

  if (answer.indexOf("Bố") > -1) {
    console.log(helloDad);
  } else if (answer.indexOf("Mẹ") > -1) {
    console.log(helloMom);
  } else if (answer.indexOf("Anh trai") > -1) {
    console.log(helloBigBro);
  } else if (answer.indexOf("Em gái") > -1) {
    console.log(helloLitSis);
  } else {
    console.log("Không quen nhá");
  }
}

greetFamillyMembers();

/** Bài tập về nhà 4: Tính số số lẻ và số số chẵn
 *
 * input: Nhập vào 3 số nguyên: firstNum, secondNum, thirdNum
 *
 * process:
 *  1. Tạo các biến input: firstNum, secondNum, thirdNum
 *  2. Tạo biến tổng số số lẻ: totalOddNum và số số chẵn: TotalEvenNum
 *  3. Kiểm ta từng biến input được nhập vào là số lẻ hay chẵn bằng phép chia lấy dư cho 2.
 *     Nếu phép chia lấy dư cho dư 1 thì số số chẵn tăng 1 và ngược lại.
 *
 * output: Xuất ra số số lẻ và số số chẵn
 *
 */

function remainder(inputNum) {
  var remainder;

  remainder = inputNum % 2;
  return remainder;
}

function verifyOddOrEven() {
  var firstNum = 32232;
  var secondNum = 3765;
  var thirdNum = 9986;

  var totalOddNum = 0;
  var totalEvenNum = 0;

  if (remainder(firstNum) === 0) {
    totalEvenNum++;
  } else {
    totalOddNum++;
  }

  if (remainder(secondNum) === 0) {
    totalEvenNum++;
  } else {
    totalOddNum++;
  }

  if (remainder(thirdNum) === 0) {
    totalEvenNum++;
  } else {
    totalOddNum++;
  }

  console.log("Số số chẵn:", totalEvenNum);
  console.log("Số số lẻ:", totalOddNum);
}

verifyOddOrEven();

/** Bài tập về nhà số 5: Tính số ngày trong tháng
 *
 * input: Nhập tháng và năm: month, year
 *
 * process:
 *  1. Tạo các biến input: month, year, days
 *  2. Kiểm tra năm nhuận hay không => số ngày của tháng 2 thay đổi
 *  3. Kiểm tra tháng được nhập vào input rồi trả ra kết quả số ngày tương ứng
 *
 * output: Xuất ra số ngày của tháng
 */

function daysOfMonth() {
  var year = 2400;
  var month = 2;

  var days;

  if (
    month === 1 ||
    month === 3 ||
    month === 5 ||
    month === 7 ||
    month === 8 ||
    month === 10 ||
    month === 12
  ) {
    days = 31;
  } else if (month === 4 || month === 6 || month === 9 || month === 11) {
    days = 30;
  } else {
    if (leapYear(year) === true) {
      days = 29;
    } else {
      days = 28;
    }
  }

  console.log("Số ngày trong tháng là:", days);
}

daysOfMonth();


/** Bài tập về nhà số 6: Xác định sinh viên ở xa trường đại học nhất
 * 
 * input: Tên và toạ dộ nhà của 3 sinh viên, toạ độ trường học
 * 
 * process:
 *    1. Tạo các biến input: name, location
 *    2. Tính khoảng cách từ nhà của từng sinh viên tới trường
 *    3. So sánh các khoảng cách vừa tính được
 *    4. Chọn khoảng cách xa nhất và tìm tên sinh viên tương ứng
 * 
 * output: Xuất ra tên sinh viên ở xa trường đại học nhất
 */

function sortLongestDistance() {

}

sortLongestDistance();


/** Bài tập về nhà số 7: Thay số âm bằng giá trị tuyệt đối
 *
 * input: Nhập vào 3 số thực: firstNum, secondNum, thirdNum
 *
 * process:
 *    1. Tạo các biến input: firstNum, secondNum, thirdNum
 *    2. Kiểm tra các số là số âm
 *    3. Thay số âm (nếu có) bằng giá trị tuyệt đối của nó (*-1)
 *
 * output: Xuất ra kết quả là 3 số trên nhưng các số âm bị thay bằng giá trị tuyệt đối
 *
 */

function reverseNegetive(num) {
  var num;

  if (num < 0) {
    num = num * -1;
    return num;
  } else {
    return num;
  }
}

function replaceNegative() {
  var firstNum = -2;
  var secondNum = -54;
  var thirdNum = 4;

  firstNum = reverseNegetive(firstNum);
  secondNum = reverseNegetive(secondNum);
  thirdNum = reverseNegetive(thirdNum);

  console.log(firstNum, secondNum, thirdNum);
}

replaceNegative();

/** Bài tập về nhà số 8: Đọc số nguyên có 3 chữ số
 *
 * input: Nhập số nguyên có 3 chữ số
 *
 * process:
 *    1. Tạo biến input: Số nhập vào: inputNumber
 *    2. Tách từng kí tự của số đã nhập thành 3 số mới: số hàng trăm (hundred), số hàng chục (ten), số hàng đơn vị (unit)
 *    3. Với mỗi vị trí, xét cách đọc từ 0-9
 *
 * output: Xuất ra cách đọc của số nguyên này
 *
 */

function readHundred(hundred) {
  switch (hundred) {
    case "1":
      return "Một trăm";
    case "2":
      return "Hai trăm";
    case "3":
      return "Ba trăm";
    case "4":
      return "Bốn trăm";
    case "5":
      return "Năm trăm";
    case "6":
      return "Sáu trăm";
    case "7":
      return "Bảy trăm";
    case "8":
      return "Tám trăm";
    case "9":
      return "Chín trăm";
    default: return "Không hợp lệ";
  }
}

function readTen(ten) {
  switch (ten) {
    case "0":
      return "linh";
    case "1":
      return "mười";
    case "2":
      return "hai mươi";
    case "3":
      return "ba mươi";
    case "4":
      return "bốn mươi";
    case "5":
      return "năm mươi";
    case "6":
      return "sáu mươi";
    case "7":
      return "bảy mươi";
    case "8":
      return "tám mươi";
    case "9":
      return "chín mươi";
    default: return "Không hợp lệ";
  }
}

function readUnit(unit) {
  switch (unit) {
    case "0":
      return " ";
    case "1":
      return "một";
    case "2":
      return "hai";
    case "3":
      return "ba";
    case "4":
      return "tư";
    case "5":
      return "năm";
    case "6":
      return "sáu";
    case "7":
      return "bảy";
    case "8":
      return "tám";
    case "9":
      return "chín";
    default: return "Không hợp lệ";
  }
}

function readNumber() {
  var inputNumber = "340";

  var hundred = readHundred(inputNumber[0]);
  var ten = readTen(inputNumber[1]);
  var unit = readUnit(inputNumber[2]);

  if (inputNumber[1] === "0" && inputNumber[2] === "0") {
    console.log(hundred);
  } else if (inputNumber[2] === "0") {
    console.log(hundred, ten);
  } else {
    console.log(hundred, ten, unit);
  }
}

readNumber();

/** Bài tập về nhà số 9: Xác định loại tam giác
 *
 * input: Nhập vào giá trị 3 cạnh của tam giác
 *
 * process:
 *    1. Tạo các biến input: các cạnh của tam giác edge0, edge1, edge2
 *    2. Đảm bảo độ dài các cạnh hợp lệ: tổng 2 cạnh bất kì lớn hơn cạnh còn lại, nếu không hợp lệ => trả lỗi
 *    3. Xét trường hợp các loại tam giác đặc biệt => đưa ra điều kiện giá trị phù hợp, còn lại là tam giác thường
 *
 * output: Xuất ra loại tam giác ứng với 3 giá trị
 */

function checkValidTriangle(edge0, edge1, edge2) {
  var edge0;
  var edge1;
  var edge2;

  if (edge0 + edge1 > edge2 && edge0 + edge2 > edge1 && edge1 + edge2 > edge0) {
    return true;
  } else {
    return false;
  }
}

function checkIsoscelesTriangle(edge0, edge1, edge2) {
  var edge0;
  var edge1;
  var edge2;

  if (edge0 === edge1 || edge0 === edge2 || edge1 === edge2) {
    return true;
  } else {
    return false;
  }
}

function checkRightTriangle(edge0, edge1, edge2) {
  var edge0;
  var edge1;
  var edge2;

  if (
    edge0 * edge0 + edge1 * edge1 === edge2 * edge2 ||
    edge0 * edge0 + edge2 * edge2 === edge1 * edge1 ||
    edge2 * edge2 + edge1 * edge1 === edge0 * edge0
  ) {
    return true;
  } else {
    return false;
  }
}

function checkObtuseTriangle(edge0, edge1, edge2) {
  var edge0;
  var edge1;
  var edge2;

  if (
    edge0 * edge0 + edge1 * edge1 < edge2 * edge2 ||
    edge0 * edge0 + edge2 * edge2 < edge1 * edge1 ||
    edge2 * edge2 + edge1 * edge1 < edge0 * edge0
  ) {
    return true;
  } else {
    return false;
  }
}

function checkTriangleType() {
  var edge0 = 6;
  var edge1 = 4;
  var edge2 = 5;

  if (checkValidTriangle(edge0, edge1, edge2) !== true) {
    console.log("Các giá trị nhập vào không phải 3 cạnh của một tam giác");
    return;
  }

  if (checkIsoscelesTriangle(edge0, edge1, edge2) === true) {
    if (checkObtuseTriangle(edge0, edge1, edge2) === true) {
      console.log("Tam giác tù cân");
    } else if (checkRightTriangle(edge0, edge1, edge2 === true)) {
      console.log("Tam giác vuông cân");
    } else if (edge0 === edge1 && edge0 === edge2) {
      console.log("Tam giác đều");
    } else {
      console.log("Tam giác cân");
    }
  } else if (checkRightTriangle(edge0, edge1, edge2) === true) {
    console.log("Tam giác vuông");
  } else if (checkObtuseTriangle(edge0, edge1, edge2) === true) {
    console.log("Tam giác tù");
  } else {
    console.log("Tam giác nhọn");
  }
}

checkTriangleType();
