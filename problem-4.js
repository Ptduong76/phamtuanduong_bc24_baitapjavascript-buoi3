/** Bài tập về nhà 4: Tính số số lẻ và số số chẵn
 *
 * input: Nhập vào 3 số nguyên: firstNum, secondNum, thirdNum
 *
 * process:
 *  1. Tạo các biến input: firstNum, secondNum, thirdNum
 *  2. Tạo biến tổng số số lẻ: totalOddNum và số số chẵn: TotalEvenNum
 *  3. Kiểm ta từng biến input được nhập vào là số lẻ hay chẵn bằng phép chia lấy dư cho 2.
 *     Nếu phép chia lấy dư cho dư 1 thì số số chẵn tăng 1 và ngược lại.
 *
 * output: Xuất ra số số lẻ và số số chẵn
 *
 */

function remainder(inputNum) {
  var remainder;

  remainder = inputNum % 2;
  return remainder;
}

function verifyOddOrEven() {
  var firstNum = 32232;
  var secondNum = 3765;
  var thirdNum = 9986;

  var totalOddNum = 0;
  var totalEvenNum = 0;

  if (remainder(firstNum) === 0) {
    totalEvenNum++;
  } else {
    totalOddNum++;
  }

  if (remainder(secondNum) === 0) {
    totalEvenNum++;
  } else {
    totalOddNum++;
  }

  if (remainder(thirdNum) === 0) {
    totalEvenNum++;
  } else {
    totalOddNum++;
  }

  console.log("Số số chẵn:", totalEvenNum);
  console.log("Số số lẻ:", totalOddNum);
}

verifyOddOrEven();
