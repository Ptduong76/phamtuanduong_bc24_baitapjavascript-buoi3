/** Bài tập về nhà số 7: Thay số âm bằng giá trị tuyệt đối
 *
 * input: Nhập vào 3 số thực: firstNum, secondNum, thirdNum
 *
 * process:
 *    1. Tạo các biến input: firstNum, secondNum, thirdNum
 *    2. Kiểm tra các số là số âm
 *    3. Thay số âm (nếu có) bằng giá trị tuyệt đối của nó (*-1)
 *
 * output: Xuất ra kết quả là 3 số trên nhưng các số âm bị thay bằng giá trị tuyệt đối
 *
 */

function reverseNegetive(num) {
  var num;

  if (num < 0) {
    num = num * -1;
    return num;
  } else {
    return num;
  }
}

function replaceNegative() {
  var firstNum = -2;
  var secondNum = -54;
  var thirdNum = 4;

  firstNum = reverseNegetive(firstNum);
  secondNum = reverseNegetive(secondNum);
  thirdNum = reverseNegetive(thirdNum);

  console.log(firstNum, secondNum, thirdNum);
}

replaceNegative();
