/** Bài tập về nhà số 2: Tìm ngày, tháng, năm kế tiếp và liền trước
 *
 * input: Nhập ngày (day), tháng (month), năm (year)
 *
 * process:
 *    1. Tạo các biến input: curentDay, curentMonth, curentYear, nextDay, nextMonth, nextYear, previousDay, previousMonth, previousYear
 *    2. Chia các trường hợp:
 *      TH1: Tháng 5, 7, 8, 10
 *      TH2: Tháng 4,6,9,11
 *      TH3: Tháng 2
 *      TH3: Tháng 3
 *    3. Với mỗi trường hợp, xét các khả năng nếu ngày được nập là ngày 1, ngày 30/31 hay các ngày khác => Suy ra ngày kế và ngày trước đó
 *    4. Với tháng 2 và tháng 3, cần xét năm nhuận, sau đó lặp lại như trên
 *
 *
 * output: Xuất ra ngày, tháng, năm tiếp theo và trước đó
 *
 */

function leapYear(year) {
  var year;
  var leapYear;

  if (
    (year % 4 === 0 && year % 100 !== 0) ||
    (year % 100 === 0 && year % 400 === 0)
  ) {
    return (leapYear = true);
  } else {
    return (leapYear = false);
  }
}

function checkNextAndPreviousDates() {
  var currentDay = 1;
  var currentMonth = 3;
  var currentYear = 2400;

  var nextDay;
  var nextMonth;
  var nextYear;

  var previousDay;
  var previousMonth;
  var previousYear;

  if (
    currentMonth === 5 ||
    currentMonth === 7 ||
    currentMonth === 8 ||
    currentMonth === 10
  ) {
    if (currentDay === 1) {
      nextDay = 2;
      nextMonth = currentMonth;
      nextYear = currentYear;

      previousDay = 30;
      previousMonth = currentMonth - 1;
      previousYear = currentYear;
    } else if (currentDay === 31) {
      nextDay = 1;
      nextMonth = currentMonth + 1;
      nextYear = currentYear;

      previousDay = 30;
      previousMonth = currentMonth;
      previousYear = currentYear;
    } else {
      nextDay = currentDay + 1;
      nextMonth = currentMonth;
      nextYear = currentYear;

      previousDay = currentDay - 1;
      previousMonth = currentMonth;
      previousYear = currentYear;
    }
  } else if (
    currentMonth === 4 ||
    currentMonth === 6 ||
    currentMonth === 9 ||
    currentMonth === 11
  ) {
    if (currentDay === 1) {
      nextDay = 2;
      nextMonth = currentMonth;
      nextYear = currentYear;

      previousDay = 31;
      previousMonth = currentMonth - 1;
      previousYear = currentYear;
    } else if (currentDay === 30) {
      nextDay = 1;
      nextMonth = currentMonth + 1;
      nextYear = currentYear;

      previousDay = 31;
      previousMonth = currentMonth;
      previousYear = currentYear;
    } else {
      nextDay = currentDay + 1;
      nextMonth = currentMonth;
      nextYear = currentYear;

      previousDay = currentDay - 1;
      previousMonth = currentMonth;
      previousYear = currentYear;
    }
  } else if (currentMonth === 1) {
    if (currentDay === 1) {
      nextDay = 2;
      nextMonth = currentMonth;
      nextYear = currentYear;

      previousDay = 31;
      previousMonth = 12;
      previousYear = currentYear - 1;
    } else if (currentDay === 31) {
      nextDay = 1;
      nextMonth = currentMonth + 1;
      nextYear = currentYear;

      previousDay = 30;
      previousMonth = currentMonth;
      previousYear = currentYear;
    } else {
      nextDay = currentDay + 1;
      nextMonth = currentMonth;
      nextYear = currentYear;

      previousDay = currentDay - 1;
      previousMonth = currentMonth;
      previousYear = currentYear;
    }
  } else if (currentMonth === 12) {
    if (currentDay === 1) {
      nextDay = 2;
      nextMonth = currentMonth;
      nextYear = currentYear;

      previousDay = 30;
      previousMonth = currentMonth - 1;
      previousYear = currentYear;
    } else if (currentDay === 31) {
      nextDay = 1;
      nextMonth = currentMonth + 1;
      nextYear = currentYear + 1;

      previousDay = 30;
      previousMonth = currentMonth;
      previousYear = currentYear;
    } else {
      nextDay = currentDay + 1;
      nextMonth = currentMonth;
      nextYear = currentYear;

      previousDay = currentDay - 1;
      previousMonth = currentMonth;
      previousYear = currentYear;
    }
  } else if (currentMonth === 2) {
    if (leapYear(currentYear) === true) {
      if (currentDay === 1) {
        nextDay = 2;
        nextMonth = currentMonth;
        nextYear = currentYear;

        previousDay = 31;
        previousMonth = currentMonth - 1;
        previousYear = currentYear;
      } else if (currentDay === 29) {
        nextDay = 1;
        nextMonth = currentMonth + 1;
        nextYear = currentYear;

        previousDay = 28;
        previousMonth = currentMonth;
        previousYear = currentYear;
      } else {
        nextDay = currentDay + 1;
        nextMonth = currentMonth;
        nextYear = currentYear;

        previousDay = currentDay - 1;
        previousMonth = currentMonth;
        previousYear = currentYear;
      }
    } else {
      if (currentDay === 1) {
        nextDay = 2;
        nextMonth = currentMonth;
        nextYear = currentYear;

        previousDay = 31;
        previousMonth = currentMonth - 1;
        previousYear = currentYear;
      } else if (currentDay === 28) {
        nextDay = 1;
        nextMonth = currentMonth + 1;
        nextYear = currentYear;

        previousDay = 27;
        previousMonth = currentMonth;
        previousYear = currentYear;
      } else {
        nextDay = currentDay + 1;
        nextMonth = currentMonth;
        nextYear = currentYear;

        previousDay = currentDay - 1;
        previousMonth = currentMonth;
        previousYear = currentYear;
      }
    }
  } else {
    if (leapYear(currentYear) === true) {
      if (currentDay === 1) {
        nextDay = 2;
        nextMonth = currentMonth;
        nextYear = currentYear;

        previousDay = 29;
        previousMonth = currentMonth - 1;
        previousYear = currentYear;
      } else if (currentDay === 31) {
        nextDay = 1;
        nextMonth = currentMonth + 1;
        nextYear = currentYear;

        previousDay = 30;
        previousMonth = currentMonth;
        previousYear = currentYear;
      } else {
        nextDay = currentDay + 1;
        nextMonth = currentMonth;
        nextYear = currentYear;

        previousDay = currentDay - 1;
        previousMonth = currentMonth;
        previousYear = currentYear;
      }
    } else {
      if (currentDay === 1) {
        nextDay = 2;
        nextMonth = currentMonth;
        nextYear = currentYear;

        previousDay = 28;
        previousMonth = currentMonth - 1;
        previousYear = currentYear;
      } else if (currentDay === 31) {
        nextDay = 1;
        nextMonth = currentMonth + 1;
        nextYear = currentYear;

        previousDay = 30;
        previousMonth = currentMonth;
        previousYear = currentYear;
      } else {
        nextDay = currentDay + 1;
        nextMonth = currentMonth;
        nextYear = currentYear;

        previousDay = currentDay - 1;
        previousMonth = currentMonth;
        previousYear = currentYear;
      }
    }
  }

  console.log(
    "Ngày, tháng, năm tiếp theo là:",
    "Ngày",
    nextDay,
    "Tháng",
    nextMonth,
    "Năm",
    nextYear
  );
  console.log(
    "Ngày, tháng, năm trước đó là:",
    "Ngày",
    previousDay,
    "Tháng",
    previousMonth,
    "Năm",
    previousYear
  );
}

checkNextAndPreviousDates();
