/** Bài tập về nhà số 1: Sắp xếp theo thứ tự tăng dần
 *
 * input: nhập 3 số nguyên: firstNum, secondNum, thirdNum
 *
 * process:
 *    1. Tạo các biến input:
 *        số thứ nhất: firstNum
 *        số thứ hai: secondNum
 *        số thứ ba: thirdNum
 *    2. So sánh số thứ nhất với số thứ 2 => chia thành 2 trường hợp
 *    3. So sánh số cond lại với 2 đầu tiên => xét các trường hợp còn lại
 *    4. In ra kết quả sắp xếp 3 số theo thứ tự ứng với từng trường hợp
 *
 * output: xuất 3 số theo thứ tự tăng dần
 */

function sortAscending() {
  var firstNum = 19762;
  var secondNum = 4677;
  var thirdNum = 49000;

  if (firstNum <= secondNum) {
    if (thirdNum <= firstNum) {
      console.log("Thứ tự tăng dần:", thirdNum, firstNum, secondNum);
    } else {
      if (thirdNum <= secondNum) {
        console.log("Thứ tự tăng dần:", firstNum, thirdNum, secondNum);
      } else {
        console.log("Thứ tự tăng dần:", firstNum, secondNum, thirdNum);
      }
    }
  } else {
    if (thirdNum <= secondNum) {
      console.log("Thứ tự tăng dần:", thirdNum, secondNum, firstNum);
    } else {
      if (thirdNum <= firstNum) {
        console.log("Thứ tự tăng dần:", secondNum, thirdNum, firstNum);
      } else {
        console.log("Thứ tự tăng dần:", secondNum, firstNum, thirdNum);
      }
    }
  }
}

sortAscending();
