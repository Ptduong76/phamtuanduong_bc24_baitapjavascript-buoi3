/** Bài tập về nhà số 8: Đọc số nguyên có 3 chữ số
 *
 * input: Nhập số nguyên có 3 chữ số
 *
 * process:
 *    1. Tạo biến input: Số nhập vào: inputNumber
 *    2. Tách từng kí tự của số đã nhập thành 3 số mới: số hàng trăm (hundred), số hàng chục (ten), số hàng đơn vị (unit)
 *    3. Với mỗi vị trí, xét cách đọc từ 0-9
 *
 * output: Xuất ra cách đọc của số nguyên này
 *
 */

function readHundred(hundred) {
  switch (hundred) {
    case "1":
      return "Một trăm";
    case "2":
      return "Hai trăm";
    case "3":
      return "Ba trăm";
    case "4":
      return "Bốn trăm";
    case "5":
      return "Năm trăm";
    case "6":
      return "Sáu trăm";
    case "7":
      return "Bảy trăm";
    case "8":
      return "Tám trăm";
    case "9":
      return "Chín trăm";
    default:
      return "Không hợp lệ";
  }
}

function readTen(ten) {
  switch (ten) {
    case "0":
      return "linh";
    case "1":
      return "mười";
    case "2":
      return "hai mươi";
    case "3":
      return "ba mươi";
    case "4":
      return "bốn mươi";
    case "5":
      return "năm mươi";
    case "6":
      return "sáu mươi";
    case "7":
      return "bảy mươi";
    case "8":
      return "tám mươi";
    case "9":
      return "chín mươi";
    default:
      return "Không hợp lệ";
  }
}

function readUnit(unit) {
  switch (unit) {
    case "0":
      return " ";
    case "1":
      return "một";
    case "2":
      return "hai";
    case "3":
      return "ba";
    case "4":
      return "tư";
    case "5":
      return "năm";
    case "6":
      return "sáu";
    case "7":
      return "bảy";
    case "8":
      return "tám";
    case "9":
      return "chín";
    default:
      return "Không hợp lệ";
  }
}

function readNumber() {
  var inputNumber = "340";

  var hundred = readHundred(inputNumber[0]);
  var ten = readTen(inputNumber[1]);
  var unit = readUnit(inputNumber[2]);

  if (inputNumber[1] === "0" && inputNumber[2] === "0") {
    console.log(hundred);
  } else if (inputNumber[2] === "0") {
    console.log(hundred, ten);
  } else {
    console.log(hundred, ten, unit);
  }
}

readNumber();
