/** Bài tập trên lớp 3: Tính thuế thu nhập cá nhân
 *
 * input: Thông tin của một cá nhân
 *    Họ tên: name
 *    Tổng thu nhập năm: totalIncome
 *    Số người phụ thuộc: totalDependant
 *
 * process:
 *    1. Tạo các biến input: name, totalIncome, totalDependant, taxableIncome, taxRate, totalIncomeTax
 *    2. Tính thu nhập chịu thuế
 *    3. Xác định mức thuế suất dựa trên thu nhập chịu thuế
 *    4. Tính thuế thu nhập cá nhân
 *
 * output: Tính và xuất ra thuế thu nhập cá nhân
 *
 */

function calcIncomeTax() {
  var name = "Cybersoft";
  var totalIncome = 260000000;
  var totalDependant = 2;
  var taxableIncome;
  var taxRate;
  var totalIncomeTax;

  taxableIncome = totalIncome - 4000000 - totalDependant * 1600000;

  if (totalIncome <= 60000000) {
    taxRate = 0.05;
  } else if (totalIncome > 60000000 && totalIncome <= 120000000) {
    taxRate = 0.1;
  } else if (totalIncome > 120000000 && totalIncome <= 210000000) {
    taxRate = 0.15;
  } else if (totalIncome > 210000000 && totalIncome <= 384000000) {
    taxRate = 0.2;
  } else if (totalIncome > 384000000 && totalIncome <= 624000000) {
    taxRate = 0.25;
  } else if (totalIncome > 634000000 && totalIncome <= 960000000) {
    taxRate = 0.3;
  } else {
    taxRate = 0.35;
  }

  totalIncomeTax = taxableIncome * taxRate;

  console.log(totalIncomeTax);
}

calcIncomeTax();
