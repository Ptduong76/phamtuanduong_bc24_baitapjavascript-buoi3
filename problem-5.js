/** Bài tập về nhà số 5: Tính số ngày trong tháng
 *
 * input: Nhập tháng và năm: month, year
 *
 * process:
 *  1. Tạo các biến input: month, year, days
 *  2. Kiểm tra năm nhuận hay không => số ngày của tháng 2 thay đổi
 *  3. Kiểm tra tháng được nhập vào input rồi trả ra kết quả số ngày tương ứng
 *
 * output: Xuất ra số ngày của tháng
 */
function leapYear(year) {
  var year;
  var leapYear;

  if (
    (year % 4 === 0 && year % 100 !== 0) ||
    (year % 100 === 0 && year % 400 === 0)
  ) {
    return (leapYear = true);
  } else {
    return (leapYear = false);
  }
}

function daysOfMonth() {
  var year = 2400;
  var month = 2;

  var days;

  if (
    month === 1 ||
    month === 3 ||
    month === 5 ||
    month === 7 ||
    month === 8 ||
    month === 10 ||
    month === 12
  ) {
    days = 31;
  } else if (month === 4 || month === 6 || month === 9 || month === 11) {
    days = 30;
  } else {
    if (leapYear(year) === true) {
      days = 29;
    } else {
      days = 28;
    }
  }

  console.log("Số ngày trong tháng là:", days);
}

daysOfMonth();
